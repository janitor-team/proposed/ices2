Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IceS
Upstream-Contact: The IceS Development Team <team@icecast.org>
Source: http://downloads.xiph.org/releases/ices/
 https://gitlab.xiph.org/xiph/icecast-ices

Files: *
Copyright: 2001-2002, Michael Smith <msmith@xiph.org>
           2001-2020, The IceS Development Team <team@icecast.org>
           2009-2012, Philipp Schafft <lion@lion.leolix.org>
License: GPL-2

Files: src/common/*
Copyright: 2014, Michael Smith <msmith@icecast.org>,
           2014, Brendan Cully <brendan@xiph.org>,
           2014, Ralph Giles <giles@xiph.org>,
           2014, Ed "oddsock" Zaleski <oddsock@xiph.org>,
           2014, Karl Heyes <karl@xiph.org>,
           2014, Jack Moffitt <jack@icecast.org>,
           2014, Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>,
           2014, Thomas Ruecker <thomas@ruecker.fi>
License: LGPL-2+

Files: src/common/avl/*
Copyright: 1995-1997, Sam Rushing <rushing@nightmare.com>
License: ISC~Rushing

Files: src/resample.h
Copyright: 2002, Simon Hosie <gumboot@clear.net.nz>
License: LGPL-2

Files: debian/*
Copyright: 2003, Keegan Quinn <ice@thebasement.org>
           2004-2016, Jonas Smedegaard <dr@jones.dk>
           2015-2020, Unit 193 <unit193@debian.org>
License: GPL-2+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: ISC~Rushing
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the name of Sam Rushing not be used
 in advertising or publicity pertaining to distribution of the software
 without specific, written prior permission.
 .
 SAM RUSHING DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 EVENT SHALL SAM RUSHING BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.
